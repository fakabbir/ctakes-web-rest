from ubuntu:18.04

ADD ./backup.sql /
ADD ./ctakes-web-rest.war /
ADD ./apache-tomcat-8.5.34.tar.gz /
WORKDIR /
